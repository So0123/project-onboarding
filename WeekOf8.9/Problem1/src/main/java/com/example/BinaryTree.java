package com.example;

import java.util.LinkedList;
import java.util.Queue;

public class BinaryTree {
    Node root;

    String levelOrder()
    {
        Queue<Node> queue = new LinkedList<Node>();
        queue.add(root);
        String traversal = "";
        while (!queue.isEmpty())
        {

            Node tempNode = queue.poll();
            traversal += tempNode.data + " ";

            if (tempNode.left != null) {
                queue.add(tempNode.left);
            }

            if (tempNode.right != null) {
                queue.add(tempNode.right);
            }
        }
        return traversal;
    }
}
