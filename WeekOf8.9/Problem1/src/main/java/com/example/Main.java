package com.example;

public class Main {

    public static void main(String[] args) {
        BinaryTree tree_level = new BinaryTree();
        tree_level.root = new Node(1);
        tree_level.root.left = new Node(20);
        tree_level.root.right = new Node(31);
        tree_level.root.left.left = new Node(442);
        tree_level.root.left.right = new Node(51);

        System.out.println(tree_level.levelOrder());


    }

}
