package com.example;

import java.util.ArrayList;
import java.util.List;

public class WordBucket {
    static List<String> wordList(String s, int size) {
        List<String> wordBucket = new ArrayList<>();
        String[] wordArray = s.split(" ");

        for (String word : wordArray) {
            if (word.length() > size) {
                return wordBucket;
            }
        }

        String str = "";

        for (String word : wordArray) {
            if (str.length() + word.length() > size) {
                wordBucket.add(str.substring(0, str.length() - 1));
                str = "";
            }
            str += word + " ";
        }
        wordBucket.add(str.substring(0,str.length() - 1));
        return wordBucket;
    }
}
