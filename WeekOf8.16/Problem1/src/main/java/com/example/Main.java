package com.example;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        WordBucket.wordList("she sells sea shells by the sea", 10).forEach(str -> System.out.println(str));
        System.out.println("");
        WordBucket.wordList("the mouse jumped over the cheese", 7).forEach(str -> System.out.println(str));
        System.out.println("");
        WordBucket.wordList("fairy dust coated the air", 1).forEach(str -> System.out.println(str));
        System.out.println("");
        WordBucket.wordList("a b c d e", 2).forEach(str -> System.out.println(str));
    }
}
