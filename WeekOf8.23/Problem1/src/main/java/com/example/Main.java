package com.example;

public class Main {
    public static void main(String[] args) {
        // long enough number
        System.out.println(CreditCardChecker.checkCard("34566542990011") + "\n");

        // short number
        System.out.println(CreditCardChecker.checkCard("3456") + "\n");

        // too long number
        System.out.println(CreditCardChecker.checkCard("12435312515351531551555") + "\n");

        // not a number
        System.out.println(CreditCardChecker.checkCard("ghrs124dsaasaa42") + "\n");

        // for other tests
        System.out.println(CreditCardChecker.checkCard("") + "\n");

        // long enough number but checkValue does not match the outcome
        System.out.println(CreditCardChecker.checkCard("1234567890123456") + "\n");

        // true
        System.out.println(CreditCardChecker.checkCard("1234567890123452") + "\n");
    }
}
