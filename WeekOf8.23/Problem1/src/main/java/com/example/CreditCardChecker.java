package com.example;

public class CreditCardChecker {
    public static boolean checkCard(String cardNum) {
        if (cardNum.length() < 14 || cardNum.length() > 19) {
            System.out.println("not valid number");
            return false;
        }
        if (!cardNum.matches("[0-9]+")) {
            System.out.println("contains more than numbers");
            return false;
        }

        String checkDigit = cardNum.substring(cardNum.length() - 1);
        cardNum = cardNum.substring(0, cardNum.length() - 1);

        String reversedNum = "";

        for (int i = 0 ; i < cardNum.length() ; i++) {
            reversedNum += "" + cardNum.charAt(cardNum.length() - i - 1);
        }

        String numAfterSelectiveDoubling = "";

        for (int i = 0 ; i < cardNum.length() ; i++) {
            int number = 0;
            if (i % 2 == 0) {
                int doubleNumber = Integer.parseInt("" + reversedNum.charAt(i)) * 2;
                if (doubleNumber > 9) {
                    String temp = "" + doubleNumber;
                    numAfterSelectiveDoubling += (Integer.parseInt(temp.substring(0,1)) + Integer.parseInt(temp.substring(1,2)));
                } else {
                    numAfterSelectiveDoubling += doubleNumber;
                }
            } else {
                numAfterSelectiveDoubling += "" + reversedNum.charAt(i);
            }
        }

        int total = 0;

        for (int i = 0 ; i < numAfterSelectiveDoubling.length() ; i++) {
            total += Integer.parseInt(numAfterSelectiveDoubling.substring(i, i+1));
        }

//        System.out.println(numAfterSelectiveDoubling);
//        System.out.println(10-total % 10);
        return Integer.parseInt(checkDigit) == 10 - total % 10;

    }
}
