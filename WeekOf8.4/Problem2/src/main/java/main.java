import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class main {
    public static void main(String[] args) {
        List<Integer> intArray = new ArrayList<>();
        intArray.add(7);
        intArray.add(4);
        intArray.add(5);
        intArray.add(3);
        intArray.add(100);
        intArray.add(6);
        intArray.add(2);
        intArray.add(18);
        intArray.add(8);
        System.out.println("Smallest value is: " + smallestValue(intArray));
        System.out.println("Largest value is: " + greatestValue(intArray));
    }

    public static int greatestValue(List<Integer> intArray) {
        Collections.sort(intArray);
        return intArray.get(intArray.size() - 1);
    }

    public static int smallestValue(List<Integer> intArray) {
        Collections.sort(intArray);
        return intArray.get(0);
    }

}
