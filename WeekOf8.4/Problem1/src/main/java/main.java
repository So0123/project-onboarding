import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class main {
    public static void main(String[] args) {
        int a = longestCommonSequence("Minneapolis","Minnesota");
        System.out.println(a);
    }

    public static int longestCommonSequence(String s1, String s2) {
        char[] c1 = s1.toLowerCase().toCharArray();
        char[] c2 = s2.toLowerCase().toCharArray();

        int[][] intArray = new int[c1.length + 1][c2.length + 1];
        for (int i = 0 ; i < c1.length + 1 ; i++) {
            intArray[i][0] = 0;
            for (int j = 0 ; j < c2.length + 1 ; j++) {
                intArray[0][j] = 0;
            }
        }
        for (int i = 1 ; i < c1.length + 1 ; i++) {
            for (int j = 1 ; j < c2.length + 1 ; j++) {

                if (c1[i-1] == c2[j-1] && intArray[i][j-1] == intArray[i-1][j]) {
                    intArray[i][j] = intArray[i-1][j-1] + 1;
                } else if (intArray[i][j-1] > intArray[i-1][j]){
                    intArray[i][j] = intArray[i][j-1];
                } else if (intArray[i][j-1] < intArray[i-1][j]) {
                    intArray[i][j] = intArray[i - 1][j];
                } else if (intArray[i][j-1] == intArray[i-1][j]) {
                    intArray[i][j] = intArray[i - 1][j];
                }
            }
        }

        return intArray[c1.length][c2.length];
    }
}
